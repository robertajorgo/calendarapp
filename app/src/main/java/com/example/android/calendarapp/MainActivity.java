package com.example.android.calendarapp;

import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    ViewPager mViewPager;
    CalendarFragmentAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mAdapter = new CalendarFragmentAdapter(this, getSupportFragmentManager());
        mViewPager.setAdapter(mAdapter);
    }
}
